Files:	

U26_1.bin - This is a dump of contents of 2532 EPROM U26 (4K) it contains Super 80 monitor V1.2 

Character Generator ROMs. These can be burnt into a 2716,2732 or 2764 EPROM to emulate the original 2513 which is almost non existant these days. Only one set is required but both can be burnt into a suitable chip to give a switchable character set.

2513l.bin - Lower Case
2513u.bin - Upper Case

