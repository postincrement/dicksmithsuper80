	$eject	48
	.Z80
hival	equ	30H
loval	equ	20H
iport	equ	0F2H
port	equ	0F0H
mask	equ	02H
led	equ	0
decide	equ	30H
cksm	equ	0F1H
len	equ	0F2H
lenh	equ	0F3H
nblks	equ	0F4H
start	equ	0F5H
starth	equ	0F6H
rrate	equ	0F9H
trate	equ	0FAH
s80in	equ	0C1BDH
s80out	equ	0C215H
getsiz	equ	0C56DH
getlen	equ	0C3F2H
getnum	equ	0C0D3H
dumpent	equ	0C4B8H			; Entry point to hex dump part.

	.phase	0CD38H
casout:	push	de
	push	hl
	call	getsiz
	pop	hl
	pop	de
	ld	a,(ix+rrate)
	cp	decide
	jp	nc,s80out
	jp	write
cassin:	push	de
	call	getsiz
	pop	de
	ld	a,(ix+rrate)
	cp	decide
	jp	nc,s80in
read:	push	de
	call	getsiz
headr1:	ld	a,20H
	out	(port),a
	ex	af,af'
	ld	de,01H
	ld	hl,0FC00H
	in	a,(iport)
	and	mask
	ld	c,a
l2:	in	a,(iport)
	and	mask
	xor	c
	jr	z,l2
	ld	c,11H
l3:	call	rbit
	jr	nc,headr1
	jr	z,headr1
	add	hl,de
	ld	c,0AH
	nop
	jr	nc,l3
	ld	a,led
	out	(port),a
	ld	c,9H
l4:	call	rbit
	jr	z,headr1
	ld	c,11
	nop
	jr	c,l4
	ld	c,10
	call	rbyte
	jr	z,err0
	ld	(ix+start),d
	ld	l,d
	ld	e,d
	ld	c,6
	call	rbyte
	jr	z,err0
	ld	(ix+starth),d
	ld	a,d
	xor	e
	ld	e,a
	ld	h,d
	ld	c,6
	call	rbyte
	jr	z,err0
	ld	(ix+len),d
	ld	a,d
	xor	e
	ld	e,a
	ld	c,6
	call	rbyte
	jr	nz,block
err0:	pop	de
err:	ld	a,26h
	out	(port),a
	scf
	ret
block:	ld	(ix+lenh),d
	ld	a,d
	xor	e
	ld	e,a
	ld	c,6
	call	rbyte
	ld	a,e
	cp	d
	jr	nz,err0
	pop	de
	ld	a,d
	or	e
	jr	z,radd
	ex	de,hl
radd:	push	hl
	pop	iy
	ld	h,(ix+lenh)
	ld	l,(ix+len)
	ld	c,l
	ld	e,0		; init checksum.
b2:	call	rbyte
	jr	z,err
	ld	(iy+0),d
	ld	a,e
	xor	d
	ld	e,a
	ld	a,l
	or	a
	ld	c,3
	jr	nz,iyad
	ex	af,af'
	out	(port),a
	xor	20H
	ex	af,af'
	ld	c,1
iyad:	inc	iy
	dec	hl
	ld	a,h
	or	l
	jr	nz,b2
	call	rbyte
	ld	h,(ix+lenh)
	ld	a,(ix+len)
	or	a
	jr	z,noinc
	inc	h
noinc:	ld	(ix+nblks),h
	ld	a,d
	xor	e
	add	a,0FFH
	ld	a,26H
	out	(port),a
	ret
;
;read 1 bit. 155.5+8*(c-1)
;usecs since last transition
;
rbit:	dec	c
	jr	nz,rbit
	in	a,(iport)
	and	mask
	ld	c,a
	ld	b,30H
r1:	in	a,(iport)
	and	mask
	cp	c
	jr	nz,r2
	djnz	r1
	xor	a
	ret
r2:	ld	a,b
	cp	(ix+rrate)
	inc	a
	ret
;
;returns 48 usec after
;transition
;
rbyte:	call	rbit
	ret	z
	rl	d
	ld	b,7H
	ld	c,0BH
rb1:	push	bc
	call	rbit
	jr	z,err1
	rl	d
	pop	bc
	ld	c,10
	djnz	rb1
	or	1		; Clear Z
	ret
err1:	pop	bc
	ret
;
; write data onto tape.
;
write:	push	hl
	push	de
	call	getsiz
	pop	de
	pop	hl
	push	hl
	ld	a,d
	xor	e
	xor	h
	xor	l
	ld	(ix+cksm),a
;
;write 5 sec header of 1's
;
	ld	hl,1000
hedr:	ld	a,0FFH
	ld	c,3
	call	wrby
	dec	hl
	ld	a,h
	or	l
	jr	nz,hedr
	ld	a,0FEH		;sync 0
	ld	c,4
	call	wrby
	pop	hl
	ld	a,l
	ld	c,5
	call	wrby
	ld	a,h
	nop
	ld	c,5
	call	wrby
	ld	a,e
	nop
	ld	c,5
	call	wrby
	ld	a,d
	nop
	ld	c,5
	call	wrby
	ld	a,(ix+cksm)
	ld	c,4
	call	wrby
	ld	(ix+cksm),0
	ld	c,1
wb1:	ld	a,(hl)
	xor	(ix+cksm)
	ld	(ix+cksm),a
	ld	a,(hl)
	call	wrby
	inc	hl
	dec	de
	ld	c,1
	ld	a,d
	or	e
	jr	nz,wb1
	ld	a,(ix+cksm)
	ld	c,2
	call	wrby
	ld	c,9
wb2:	dec	c
	jr	nz,wb2
	ld	a,hival
	out	(port),a
b3:	dec	c
	jr	nz,b3
	ld	a,36H
	out	(port),a
	ret
;
;write one byte. 8*(c-1)+30
; = no. of usecs to start of
;next bit.
;
wrby:	ld	b,8H
bit:	sla	a
	push	af
	jr	c,one
	nop
w1:	dec	c
	jr	nz,w1
	ld	a,hival
	out	(port),a
	ld	c,0CH
w2:	dec	c
	jr	nz,w2
	ld	c,(ix+trate)
bw1:	dec	c
	jr	nz,bw1
	ld	a,loval
	out	(port),a
	ld	c,(ix+trate)
bw2:	dec	c
	jr	nz,bw2
	pop	af
	ld	c,9
	djnz	bit
	ret
;
; Must call back to 'wrby' after 54 usecs.
;
one:	dec	c
	jr	nz,one
	ld	a,hival
	out	(port),a
	ld	c,1AH
w4:	dec	c
	jr	nz,w4
	ld	c,(ix+trate)
	sla	c
bw3:	dec	c
	jr	nz,bw3
	ld	a,loval
	out	(port),a
	ld	c,(ix+trate)
	sla	c
bw4:	dec	c
	jr	nz,bw4
	ld	c,14
w5:	dec	c
	jr	nz,w5
	pop	af
	ld	c,9
	djnz	bit
	ret
;
;return with 57.5 usecs until
;start of next bit.
;
;
; Come here from C481
;
extra:	cp	'E'
	jp	z,0C4FBH
	cp	'3'
	jr	z,set300
	cp	'6'
	jr	z,set600
	cp	'8'
	jr	z,st3000
	cp	'9'
	jr	z,st4500
	cp	'M'
	jr	z,move
	cp	'F'
	jr	z,fill
	cp	'B'
	jp	z,0D003H
	cp	'*'
	jp	z,0D000H
	cp	'R'
	jr	z,backup
	cp	'A'
	jr	z,samadd
	cp	'P'
	jr	z,setpag
	jp	0C485H

set300:	ld	bc,0F804H
	jr	setcas
set600:	ld	bc,7C02H
	jr	setcas
st3000:	ld	bc,1F08H
	jr	setcas
st4500:	ld	bc,2701H
setcas:	ld	(ix+0F9H),b
	ld	(ix+0FAH),c
	ret
move:	call	stlen
	inc	bc
	ldir
	ret
fill:	call	stlen
	ld	(hl),e
	ld	d,h
	ld	e,l
	inc	de
	ld	a,b
	or	c
	ret	z
	ldir
	ret
backup:	ld	a,(ix+0F7H)
	sub	80H
dump:	ld	(ix+0F7H),a
	jr	nc,sampag
	dec	(ix+0F8H)
sampag:	jp	dumpent
samadd	ld	a,(ix+0F7H)
	sub	40H
	jr	dump
setpag:	call	getlen
	call	getnum
	ld	a,l
	out	(0F1H),a
	ret
;
;Get start address & no. of bytes from buffer.
;
stlen:	call	getlen		;de^ buffer start, hl^ end,
	push	hl		;b=number of chars.
	call	getnum
	pop	de
	push	hl		;Push start address.
	ex	de,hl		;hl points to next number.
	call	getlen
	push	hl		;Pointer to third number.
	call	getnum		;Get end address in hl.
	pop	de		;de now points to last number.
	pop	bc		;Restore start address.
	or	a
	sbc	hl,bc		;Get no. of bytes-1
	push	hl		;Push no. of bytes.
	push	bc		;First byte.
	ex	de,hl		;Set up hl for last number
	call	getlen
	call	getnum
	push	hl		;Destination.
	pop	de
	pop	hl
	pop	bc
	ret
;
; Return with hl=start, bc=number-1, de=last number.
;
;---------------------------------------------
;
; Come here from C527
;
revent:	cp	13
	jp	z,0C52BH
	cp	'R'		;Reverse inst.
	jp	nz,0C52FH
	pop	hl
	dec	hl
	jp	0C501H
;
; Come here from C6C8.
;
	ld	a,b
	cp	7
	jr	nz,notbel
	push	bc
	ld	a,22H
	ld	b,0
bel1:	ld	c,52H
bel2:	dec	c
	jr	nz,bel2
	out	(port),a
	xor	18H
	djnz	bel1
	ld	a,26H
	out	(port),a
	pop	bc
notbel:	jp	0C56DH
	end
